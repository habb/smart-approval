  //
 //  WebService.swift
 //  JSON
 //
 //  Created by haya habbous on 2/25/18.
 //  Copyright © 2018 haya habbous. All rights reserved.
 //

 import Foundation



 public class WebService{
     
     
     
     
     //User endpoint
     
     
    
    static func Authenticate(username: String ,password: String ,completeion: @escaping ([String:Any]) -> Void ) {
        let webResource = "api/Authenticate"
        var parametersArray:[[String:Any]] = [[:]]

        
        
        RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
            
            completeion(json)
            
        })
    }
    
    static func OTP(UserID: String ,OTPCode: String ,completeion: @escaping ([String:Any]) -> Void ) {
        let webResource = "api/OTP"
        var parametersArray:[[String:Any]] = [[:]]

        parametersArray.append(["UserID":UserID])
        parametersArray.append(["OTPCode":OTPCode])
        
        
        RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
            
            completeion(json)
            
        })
    }
    
    static func Token(UserID: String ,Token: String ,completeion: @escaping ([String:Any]) -> Void ) {
        let webResource = "api/Token"
        var parametersArray:[[String:Any]] = [[:]]

        parametersArray.append(["Token":Token])
        parametersArray.append(["UserID":UserID])
        
        RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
            
            completeion(json)
            
        })
    }
    
    static func BiometricAuthentication(UserID: String ,completeion: @escaping ([String:Any]) -> Void ) {
        let webResource = "api/BiometricAuthentication"
        var parametersArray:[[String:Any]] = [[:]]

        //parametersArray.append(["Token":Token])
        parametersArray.append(["UserID":UserID])
        
        RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
            
            completeion(json)
            
        })
    }
     static func EmailOTP(UserID: String ,completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "api/EmailOTP"
         var parametersArray:[[String:Any]] = [[:]]

         //parametersArray.append(["Token":Token])
         parametersArray.append(["UserID":UserID])
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     static func Create(username: String ,password: String,fullname: String,mobile: String ,completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "Account/Create"
         var parametersArray:[[String:Any]] = [[:]]

         parametersArray.append(["Username":username])
         parametersArray.append(["Password":password])
         parametersArray.append(["MobileNo":mobile])
         parametersArray.append(["FullName":fullname])
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
    /*
     static func DepartmentSubServices(username: String ,password: String ,completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "DepartmentSubServices"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     static func DepartmentServices(completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "DepartmentServices"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     static func getEServices(completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "EServices"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     
     static func getInnerEServices(completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "UserInnerServices"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     
     static func getMyTasks(status: String , completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "MyTasks"
         var parametersArray:[[String:Any]] = [[:]]

         parametersArray.append(["status":status])
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     
     static func getMyDocuments( completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "MyDocuments"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     
     static func getAllTranactions( completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "AllTranactions"
         var parametersArray:[[String:Any]] = [[:]]

         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     
     static func login(username: String ,password: String , completetion: @escaping ([String:Any]) -> Void) {
      
         let webResource = "Users"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["UserName": username])
         parametersArray.append(["Password": password])
         parametersArray.append(["AuthType": "User"])
         parametersArray.append(["BuildID": "1"])
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
     }*/
     /*
     static func DepartmentSubServicesByMainServices(username: String ,password: String ,completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "DepartmentSubServices/ByMainServiceId"
         var parametersArray:[[String:Any]] = [[:]]

         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     static func getMainServices(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "mainServices"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getVisaInformation(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getVisaInformation"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getNews(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getNews"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getNotification(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getNotification"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getPaymentRequest(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getPaymentRequest"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getServices(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "Services"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     ///------------Register / Reset PAssword
     static func checkLicenseNo(license_no: String ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "checkLicenseNo"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["license_no":license_no])
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func sendOTP(mobile_no: String ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "sendOTP"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["mobile_no":mobile_no])
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func RegisterNewInvestor(email: String ,password: String ,caption:String ,tradeLicenseNo: String, MobileNo: String ,MobileOTP: String ,LinkedCompanyCode: String ,MobileNoEndsWith: String,completeion: @escaping ([String:Any]) -> Void ) {
         let webResource = "RegisterNewInvestor"
         var parametersArray:[[String:Any]] = [[:]]
         
         parametersArray.append(["email":email]);
         parametersArray.append(["password":password]);
         parametersArray.append(["caption":caption]);
         parametersArray.append(["tradeLicenseNo":tradeLicenseNo]);
         parametersArray.append(["MobileNo":MobileNo]);
         parametersArray.append(["MobileOTP":MobileOTP]);
         parametersArray.append(["LinkedCompanyCode":LinkedCompanyCode]);
         parametersArray.append(["MobileNoEndsWith":MobileNoEndsWith]);
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "", completion: { (json) in
             
             completeion(json)
             
         })
     }
     static func getLicense(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "License"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     
     static func getSubmittedRequest(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "submittedRequests"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getDraftRequest(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "draftRequests"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getRequieredDocuments(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getRequieredDocuments"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getRequiredFields(company_code: String ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getRequiredFields"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["company_code":company_code]);
      
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getUserInformation(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getUserInformation"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getStatment(company_code: String , date_from:String, date_to:String  ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getStatment"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         parametersArray.append(["companyCode":company_code]);
         parametersArray.append(["dateFrom":date_from]);
         parametersArray.append(["dateTo":date_to]);
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getRemark(company_code: String , date_from:String, date_to:String  ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "remark"
         var parametersArray: [[String:Any]] = [[:]]
         

         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func payInvoices(year: String ,month: String ,day: String , company_code: String,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "payInvoices"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         parametersArray.append(["year":year]);
         parametersArray.append(["month":month]);
         parametersArray.append(["day":day]);
         parametersArray.append(["companyCode":company_code]);
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     static func getAuditReport (company_code: String,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getAuditReport"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         parametersArray.append(["company_code":company_code]);
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getEchannelMessage (company_code: String,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getEchannelMessage"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         parametersArray.append(["company_code":company_code]);
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     
     
     static func getDownloadStatmentId(recept_number: String  ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "DPAjax.aspx/BLLibraryValue"
         var parametersArray: [[String:Any]] = [[:]]
         
      
         parametersArray.append(["BLLibrary": "SAIFInvestorsPortal.GetGenesisInvoice, SAIFInvestorsPortal"]);
         parametersArray.append(["LibParameter":"SAIF/\(recept_number)"]);
   
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
             completetion(json)
         }
         
     }
     static func getDownloadStatmentForAll(startDate: String, endDate: String ,completetion: @escaping ([String:Any]) -> Void) {
            
         
         
      
           let webResource = "DPAjax.aspx/BLLibraryValue"
           var parametersArray: [[String:Any]] = [[:]]
           
         let stat = UserDefaults.standard.object(forKey: "companyCode") as? String  ?? ""
        
           parametersArray.append(["BLLibrary": "SAIFInvestorsPortal.GetGenesisStatement, SAIFInvestorsPortal"]);
         parametersArray.append(["LibParameter":"\(startDate)|\(endDate)|\(stat)"]);
     
           
           RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "") { (json) in
               completetion(json)
           }
           
       }
       
     
     
     /////------------
     static func signup(firstName: String ,lastName: String , email: String , username: String ,
                        emiratesId: String ,phone: String ,emirate: String , password: String ,address:String ,gender: String ,nationality: String  ,completeion: @escaping ([String:Any]) -> Void  ) {
     
      
         let webResource = "signup"
         var parametersArray:[[String:Any]] = [[:]]
         
         parametersArray.append(["first_name":firstName]);
         parametersArray.append(["last_name":lastName]);
         parametersArray.append(["emirates_id":emiratesId]);
         parametersArray.append(["phone":phone]);
         parametersArray.append(["email":email]);
         parametersArray.append(["password":password]);
         parametersArray.append(["username":username]);
         parametersArray.append(["emirate":emirate]);
         parametersArray.append(["address":address]);
         parametersArray.append(["gender":gender]);
         parametersArray.append(["nationality":nationality]);
         
         
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent", completion: { (json) in
             
             completeion(json)
             
         })

         
     }
     
     
     static func login(username: String ,password: String , completetion: @escaping ([String:Any]) -> Void) {
      
         let webResource = "login"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["username": username])
         parametersArray.append(["password": password])
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent") { (json) in
             completetion(json)
         }
     }
     
     static func logout(username: String ,completetion: @escaping ([String:Any]) -> Void) {
      
         let webResource = "logout"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["username": username])

         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent") { (json) in
             completetion(json)
         }
     }
     
     static func getGenders(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getGenders"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent") { (json) in
             completetion(json)
         }
         
     }
     static func getNationalities(completetion: @escaping ([String:Any]) -> Void) {
         let webResource = "getNationalities"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent") { (json) in
             completetion(json)
         }
     }
     
     static func getEmirates(completetion: @escaping ([String:Any]) -> Void) {
         let webResource = "getEmirates"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "parent") { (json) in
             completetion(json)
         }
     }
     
     
     static func createChild(parent_id: String ,first_name: String ,
                             last_name: String ,nationality: String ,
                             gender: String ,mobile: String ,
                             email: String ,emirates_id: String ,
                             emirate: String ,address: String ,
                             data_of_birth: String ,child_picture: String ,completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "createChild"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["parent_id":parent_id ])
         parametersArray.append(["first_name": first_name])
         parametersArray.append(["last_name": last_name])
         parametersArray.append(["nationality": nationality])
         parametersArray.append(["gender": gender])
         parametersArray.append(["mobile": mobile])
         parametersArray.append(["email": email])
         parametersArray.append(["emirates_id": emirates_id])
         parametersArray.append(["emirate": emirate])
         parametersArray.append(["address": address])
         
         parametersArray.append(["data_of_birth": data_of_birth])
         parametersArray.append(["child_picture": child_picture])
        
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "child") { (json) in
             completetion(json)
         }
     }
     
     static func insertNewObjective(objective_text_ar: String ,objective_text_en: String ,min_age: String , max_age: String ,user_id
         :String ,child_id: String ,aspect_id: String,routine_id: String ,completetion: @escaping ([String:Any]) -> Void) {
      
         let webResource = "insertObjective"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["objective_text_ar": objective_text_ar])
         parametersArray.append(["objective_text_en": objective_text_en])
         parametersArray.append(["min_age": min_age])
         parametersArray.append(["max_age": max_age])
         parametersArray.append(["user_id": user_id])
         parametersArray.append(["child_id": child_id])
         parametersArray.append(["aspect_id": aspect_id])
         
         parametersArray.append(["routine_id": routine_id])
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     static func insertCenetrChild(centerId: String ,child_id: String , completetion: @escaping ([String:Any]) -> Void) {
      
         let webResource = "insertCenterChild"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["center_id": centerId])
         parametersArray.append(["child_id": child_id])
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     static func getChildsForParent(isCenter: Bool ,completetion: @escaping ([String:Any]) -> Void) {
         
         var webResource = ""
         if isCenter {
             webResource = "getChildrenForCenter"
         }else {
             webResource = "getChildsForParent"
         }
      
         var parametersArray: [[String:Any]] = [[:]]
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "child") { (json) in
             completetion(json)
         }
     }
     
     static func searchChild(emiratesId:String , completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "searchChild"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["emiratesId":emiratesId])
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "child") { (json) in
             completetion(json)
         }
     }
     
     static func getChild(childId:String , completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getChildfull"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["childId":childId])
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "child") { (json) in
             completetion(json)
         }
     }
     
     
     static func getRoutines(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getRoutines"
         var parametersArray: [[String:Any]] = [[:]]
         
        
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     
     static func getAspects( completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getAspects"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     
     static func deleteChild(childId:String , completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "deleteChild"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["childId":childId])
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "child") { (json) in
             completetion(json)
         }
     }
     
     
     
     static func getSurveysList(completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getSurveysList"
         var parametersArray: [[String:Any]] = [[:]]
         
         parametersArray.append(["age": "2"])
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     
     static func getQuestions(surveyId: String , completetion: @escaping ([String:Any]) -> Void) {
         
         let webResource = "getQuestions"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         parametersArray.append(["survey_id":surveyId])
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey", completion: { (json) in
             completetion(json)
         })
     }
     
     static func submitAnswers(survey_id: String ,child_id:String ,answers: NSMutableArray ,completetion: @escaping ([String:Any]) -> Void) {
         
         
         guard let answersJson = try? JSONSerialization.data(withJSONObject: answers, options: []) else {return }
         
         guard let answersString = String(data: answersJson, encoding: String.Encoding.utf8) else {return}
         
         let webResource = "submitAnswers"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["survey_id":survey_id])
         parametersArray.append(["child_id":child_id])
         
         
         parametersArray.append(["answers":answersString])
         
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     static func submitObjective(objectiveId: String ,taskId:String ,childId:String ,result: String  ,completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "insertObjectiveChild"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["objective_id":objectiveId])
         parametersArray.append(["task_id":taskId])
         parametersArray.append(["child_id":childId])
         parametersArray.append(["result":result])
         
         
         
         
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     static func answerObjectiveChild(objectiveId: String ,childId:String ,result: String  ,completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "answerObjectiveChild"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["objective_id":objectiveId])
         parametersArray.append(["child_id":childId])
         parametersArray.append(["result":result])
         
         
         
         
         
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "survey") { (json) in
             completetion(json)
         }
     }
     static func getStories( completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "getStories"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "stories") { (json) in
             completetion(json)
         }
     }
     /*
     static func getServices( completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "getServices"
         var parametersArray: [[String:Any]] = [[:]]
         
         
         
         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "Eservices") { (json) in
             completetion(json)
         }
     }*/
     
     
     //New Api's
     
     
     static func authenticateUser(username:String ,password:String , completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "authenticate"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["username":username])
         parametersArray.append(["password":password])
         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "user") { (json) in
             completetion(json)
         }
     }
     
     static func forgetPassword(emailAddress:String, completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "forgetPassword"
         var parametersArray: [[String:Any]] = [[:]]
         parametersArray.append(["email_address":emailAddress])

         
         RequestResponseWebServices.requestByPOST(WebServiceUrl: webResource, params: &parametersArray, endpoint: "user") { (json) in
             completetion(json)
         }
     }
     
     static func getSecurityQuestions( completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "getSecurityQuestions"
         var parametersArray: [[String:Any]] = [[:]]
         

         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "user") { (json) in
             completetion(json)
         }
     }
     
     static func RetrieveGender( completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "getGenders"
         var parametersArray: [[String:Any]] = [[:]]
         

         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "user") { (json) in
             completetion(json)
         }
     }
     
     static func getCountries( completetion: @escaping ([String:Any]) -> Void) {
         
         
        
         
         let webResource = "getCountries"
         var parametersArray: [[String:Any]] = [[:]]
         

         
         RequestResponseWebServices.requestByGET(WebServiceUrl: webResource, params: &parametersArray, endpoint: "user") { (json) in
             completetion(json)
         }
     }
     */
 }
