//
//  SAIFZONEUser.swift
//  smart approval
//
//  Created by macbook pro on 05/12/2020.
//  Copyright © 2020 Datacellme. All rights reserved.
//

import Foundation
class SAIFZONEUser: NSObject {
    
    var userID: String = ""
    var username: String = ""
    var tokenID: String = ""
    override init() {
        
    }
    init(userID: String , token: String ) {
        
        

        //var sArray = data.components(separatedBy: "|")
        
        
        
        self.userID = userID
        self.tokenID = token
        
        
        
        UserDefaults.standard.set(userID, forKey: AppConstants.SaifZoneUserID)
        UserDefaults.standard.set(token, forKey: AppConstants.SaifZoneUserToken)
        
    }
    
    static func getSaifZoneUser()-> SAIFZONEUser?{
           
        guard let userID:String = UserDefaults.standard.object(forKey: AppConstants.SaifZoneUserID) as? String else {
        
            return nil
           
        }
           guard let userToken:String = UserDefaults.standard.object(forKey: AppConstants.SaifZoneUserToken) as? String else {
           
               return nil
              
           }
        
        let appUser:SAIFZONEUser = SAIFZONEUser()

        
        appUser.userID = userID
        appUser.tokenID =  userToken
        
           
           
          
        return appUser
      
    }
}
