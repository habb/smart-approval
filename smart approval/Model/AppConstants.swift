//
//  AppConstants.swift
//  smart approval
//
//  Created by macbook pro on 05/12/2020.
//  Copyright © 2020 Datacellme. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AppConstants: NSObject {
    
    static let WEB_URL: String = AppConstants.isLive ?  "https://esig.saif-zone.com/" : "https://esigdev.saif-zone.com/"//"http://172.16.131.94/api/"
    //static let WEB_URL_LIVE: String =  "https://esig.saif-zone.com/api/"
    static let SaifZoneUserData = "SaifZoneUserData"
    static let SaifZoneUserID = "SaifZoneUserID"
    static let SaifZoneUserToken = "SaifZoneUserToken"
    static let FCMToken = "SaifZoneUserToken"
    static let isLive: Bool = true
    
    
    static var APPUSERNAME: String = ""
    static var PASSWORD: String = ""
    
    
    static var IS_COMING_FROM_LOGOUT: Bool = false
    
    
    
    static var BROWN_COLOR = UIColor(red: 144.0/156.0, green: 126.0/156.0, blue: 88.0/156.0, alpha: 1.0)
    static var LIGHT_BROWN_COLOR = UIColor(red: 209.0/156.0, green: 126.0/156.0, blue: 88.0/156.0, alpha: 1.0)
    static func save(username: String , password: String) {
       
       
         
         guard let appDelegate =
         
             UIApplication.shared.delegate as? AppDelegate else {
         
                 return
       
         }
       
       
         // 1
       
         let managedContext = appDelegate.persistentContainer.viewContext
       
       // 2
      
         let entity = NSEntityDescription.entity(forEntityName: "Person",in: managedContext)!
       
       
         let person = NSManagedObject(entity: entity, insertInto: managedContext)
       
       // 3
      
         person.setValue(username, forKeyPath: "username")
         person.setValue(password, forKeyPath: "password")
       
       // 4
       
         do {
         
             try managedContext.save()
         
             //people.append(person)
      
         } catch let error as NSError {
         
             print("Could not save. \(error), \(error.userInfo)")
      
         }
    
     }
    
    static func saveIsEnabled() {
       
       
         
         guard let appDelegate =
         
             UIApplication.shared.delegate as? AppDelegate else {
         
                 return
       
         }
       
       
         // 1
       
         let managedContext = appDelegate.persistentContainer.viewContext
       
       // 2
      
         let entity = NSEntityDescription.entity(forEntityName: "Faceid",in: managedContext)!
       
       
         let person = NSManagedObject(entity: entity, insertInto: managedContext)
       
       // 3
      
         person.setValue(true, forKeyPath: "isEnabled")
         
       
       // 4
       
         do {
         
             try managedContext.save()
         
             //people.append(person)
      
         } catch let error as NSError {
         
             print("Could not save. \(error), \(error.userInfo)")
      
         }
    
     }
    
    
    
    
    static func deleteAllData(_ entity:String) {
        //1
         
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
              
                    return
        
            }
          
          let managedContext = appDelegate.persistentContainer.viewContext
          
          //2
          let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
      
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
}
