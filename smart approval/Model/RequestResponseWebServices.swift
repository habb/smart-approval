  //
 //  RequestResponseWebServices.swift
 //  JSON
 //
 //  Created by haya habbous on 2/25/18.
 //  Copyright © 2018 haya habbous. All rights reserved.
 //

 import Foundation

  import JGProgressHUD

  
  
  class RequestResponseWebServices:NSObject ,URLSessionDelegate , URLSessionDataDelegate{
     
     
     public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Swift.Void) {
         completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))
     }
     
     func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
         
         
         
     }
     enum SerializationError: Error {
         case missing(String)
         case invalid(String,Any)
     }
     
     
  
     
     static func requestByPOST(WebServiceUrl: String ,params:inout [[String:Any]] ,endpoint:String ,completion: @escaping ([String:Any]) -> Void){
         
         var urlString: String!
         
         
         urlString = (AppConstants.WEB_URL + WebServiceUrl)
        
         
         
         
         let user = SAIFZONEUser.getSaifZoneUser()
         
         //set headesr
         
         
         var postString: String = ""
         
         
         //we need to check internet connection
         for parameter in params {
             for (key,value) in parameter {
                 
                 if WebServiceUrl == "xmpp-users/set-contact-list"{
                     postString = postString + "\"\(key)\" : \(value),"
                     
                 }else{
                     if let v = value as? [String] {
                         postString = postString + "\"\(key)\" : \(v),"
                     }else if key == "answers" {
                         postString = postString + "\"\(key)\" : \(value),"
                     }else {
                     
                         postString = postString + "\"\(key)\" : \"\(value)\","
                     }
                     
                 }
                 
             }
         }
         
         if postString.count > 0{
             postString = postString.substring(to: postString.index(before: postString.endIndex))
             postString = "{\(postString)}"
         }
         
         print(urlString!)
         print(postString)
         var jsonResponse:[String:Any] = [:]
         
         
         urlString = replaceCharacter(InString: urlString!)
         var request = URLRequest(url: URL(string: urlString)!)
         let postData = postString.data(using: .utf8)
         
         
         let langStr = Locale.current.languageCode
         request.addValue(langStr ?? "en", forHTTPHeaderField: "language");
         
         request.addValue("application/json", forHTTPHeaderField: "Content-Type");
         //request.addValue(user?.userID ?? "", forHTTPHeaderField: "TokenText")
         //request.addValue("1231423a", forHTTPHeaderField: "X-UID")
         
         
      
        
        //let username = "testuser1"
        //let password = "admin158"
        
        
        let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String
        let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String
        let loginString = String("\(username):\(password)")//NSString(format: "%@:%@", username, password)
        let loginData = loginString.data(using: .utf8)!
        let base64LoginString = loginData.base64EncodedString()
        //let base64LoginString = loginData.base64EncodedStringWithOptions([])
         
        
        let deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
        request.addValue(deviceId, forHTTPHeaderField: "X-UID")
         
         request.timeoutInterval = 30
         request.httpMethod = "POST"
         request.httpBody = postData!
         
        
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
         
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         let reachable = appDelegate.reach
          if reachable!.connection == .wifi || reachable!.connection == .cellular {
             
             
         
             
             let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error? ) in
                 //var forecastArray:[String] = []
                 if let data = data {
                     do{
                         if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]{
                             
                             jsonResponse = json
                             
                             completion(jsonResponse)
                             
                         }
                     }catch{
                         
                         jsonResponse["code"] = "-2222"
                         jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                         completion(jsonResponse)
                         
                         print(error.localizedDescription)
                     }
                 }else{
                     jsonResponse["code"] = "-2222"
                     jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                     completion(jsonResponse)
                 }
                 
                 
             }
             
             task.resume()
         }else{
             jsonResponse["code"] = "-222"
             jsonResponse["message"] = "no internet connection"
             completion(jsonResponse)
         }
         
         
         
         
         
     }
     
     static func requestByGET(WebServiceUrl: String ,params:inout [[String:Any]] ,endpoint:String ,completion: @escaping ([String:Any]) -> Void){
         
         
         
         let user = SAIFZONEUser.getSaifZoneUser()
         var urlString: String!
         if endpoint.count > 0 {
             urlString = AppConstants.WEB_URL + endpoint + "/" + WebServiceUrl
         }else {
             urlString = AppConstants.WEB_URL + WebServiceUrl + "/"
         }
         
         
         
         if params.count > 0 {
             for parameter in params {
                 for (key,value) in parameter {
                     
                     urlString = urlString + "\(value)/"
                 }
             }
         }
         
         
         //set headesr
         


         
         if params.count > 1 {
             urlString = urlString.substring(to: urlString.index(before: urlString.endIndex))
         }
         
         
         print(urlString)
  
         var jsonResponse:[String:Any] = [:]
         
         
         urlString = replaceCharacter(InString: urlString)
         var request = URLRequest(url: URL(string: urlString)!)
         
         
         
         let langStr = Locale.current.languageCode
         request.addValue(langStr ?? "en", forHTTPHeaderField: "language");
        
         request.addValue("application/json", forHTTPHeaderField: "Content-Type");
         //request.addValue(user?.userID ?? "", forHTTPHeaderField: "TokenText")
        
        let deviceId = UIDevice.current.identifierForVendor?.uuidString ?? ""
         request.addValue(deviceId, forHTTPHeaderField: "X-UID")
         
        
        //print("X-FBID is : \(UserDefaults.standard.value(forKey: AppConstants.FCMToken))")
        request.addValue(UserDefaults.standard.value(forKey: AppConstants.FCMToken) as? String ?? "" , forHTTPHeaderField: "X-FBID")
        print(deviceId)
         //let username = "testuser1"
         //let password = "admin158"
        
        let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String ?? ""
        let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String ?? ""
         let loginString = String("\(username):\(password)")//NSString(format: "%@:%@", username, password)
         let loginData = loginString.data(using: .utf8)!
         let base64LoginString = loginData.base64EncodedString()
         
         request.timeoutInterval = 30
         request.httpMethod = "GET"
   
        request.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
         
         
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         let reachable = appDelegate.reach
         if reachable!.connection == .wifi || reachable!.connection == .cellular {
             
             let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error? ) in
                 //var forecastArray:[String] = []
                 if let data = data {
                     do{
                         if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]{
                             
                             jsonResponse = json
                             
                             completion(jsonResponse)
                             
                         }
                     }catch{
                         
                         jsonResponse["code"] = "-2222"
                         jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                         completion(jsonResponse)
                         
                         print(error.localizedDescription)
                     }
                 }else{
                     jsonResponse["code"] = "-2222"
                     jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                     completion(jsonResponse)
                 }
                 
                 
             }
             
             task.resume()
         }else{
             jsonResponse["code"] = "-222"
             jsonResponse["message"] = "no internet connection"
             completion(jsonResponse)
         }
         
         
         
         
         
     }
     
     
     
     static func requestByDelete(WebServiceUrl: String ,params:inout [[String:Any]] ,endpoint:String ,completion: @escaping ([String:Any]) -> Void){
            
            
            var urlString: String = AppConstants.WEB_URL + endpoint + "/" + WebServiceUrl + "/"
            
            if params.count > 0 {
                for parameter in params {
                    for (key,value) in parameter {
                        
                        urlString = urlString + "\(value)/"
                    }
                }
            }
            
            
            //set headesr
            

            
            
            urlString = urlString.substring(to: urlString.index(before: urlString.endIndex))
            
            print(urlString)
     
            var jsonResponse:[String:Any] = [:]
            
            
            urlString = replaceCharacter(InString: urlString)
            var request = URLRequest(url: URL(string: urlString)!)
            
            
            
            let langStr = Locale.current.languageCode
            request.addValue(langStr ?? "en", forHTTPHeaderField: "language");
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type");
            
            
            request.timeoutInterval = 30
            request.httpMethod = "Delete"
         
            
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let reachable = appDelegate.reach
            if reachable!.connection == .wifi || reachable!.connection == .cellular {
                
                let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error? ) in
                    //var forecastArray:[String] = []
                    if let data = data {
                        do{
                            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]{
                                
                                jsonResponse = json
                                
                                completion(jsonResponse)
                                
                            }
                        }catch{
                            
                            jsonResponse["code"] = "-2222"
                            jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                            completion(jsonResponse)
                            
                            print(error.localizedDescription)
                        }
                    }else{
                        jsonResponse["code"] = "-2222"
                        jsonResponse["message"] = "Unable to connect to the server\n Please try again later"
                        completion(jsonResponse)
                    }
                    
                    
                }
                
                task.resume()
            }else{
                jsonResponse["code"] = "-222"
                jsonResponse["message"] = "no internet connection"
                completion(jsonResponse)
            }
            
            
            
            
            
        }

     static func replaceCharacter(InString: String) ->String{
         let newString = InString.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
         
         return newString
     }
     
     //---------------------------------------------------------------------------------------------------------------------
     static func urlencode(_ enteredString: String?) -> String? {
         var output = ""
         let source = enteredString?.utf8CString
         let sourceLen = source?.count
         for i in 0 ..< sourceLen! {
             let thisChar = source![i]
             if thisChar == " ".utf8CString.first!  {
                 output += "+"
             }
             else if thisChar == ".".utf8CString.first! || thisChar == "-".utf8CString.first! || thisChar == "_".utf8CString.first! || thisChar == "~".utf8CString.first! || (thisChar >= "a".utf8CString.first! && thisChar <= "z".utf8CString.first!) || (thisChar >= "A".utf8CString.first! && thisChar <= "Z".utf8CString.first!) || (thisChar >= "0".utf8CString.first! && thisChar <= "9".utf8CString.first!) {
                 output += "\(thisChar)"
             }
             else {
                 output += String(format: "%%%02X", thisChar)
             }
         }
         return output
     }
     /*
     static func getHash(forText text: String?) -> String? {
         var hash = (text?.sha256())!
         hash = hash.replacingOccurrences(of: " ", with: "")
         hash = hash.replacingOccurrences(of: "<", with: "")
         hash = hash.replacingOccurrences(of: ">", with: "")
         print(hash)
         return hash
     }*/
     //---------------------------------------------------------------------------------------------------------------------
     
     /*
     static func uploadUsingAFNetworkingFromUrl(view: UIView ,type: String ,galleryItems:[Gallery],completation: @escaping ([String:Any] , NSMutableArray)->Void){
        
         
         
         
         let uploadURL = AppConstants.WEB_ENGILINK_UPLOAD_URL
         var result = [String:Any]()
         let linkuser = EngiLinkUser.getEngiLinkUser()
         
         
         let customeView  = ProgressBannerView(view: view)
         
         let banner = NotificationBanner(customView: customeView)
         //banner.delegate = self
         banner.duration = 3000
         
         
         banner.isUserInteractionEnabled = false
         let request = AFHTTPRequestSerializer().multipartFormRequest(withMethod: "POST", urlString: uploadURL, parameters: nil, constructingBodyWith: { (formData) in
             do {
                 
                 for item in galleryItems {
                     
                     
                     var theURL = URL(string: item.path)
                     if !theURL!.isFileURL {
                         theURL = URL(fileURLWithPath: item.path)
                     }
                     //let uploadURL = AppConstants.FILE_UPLOAD_BASE_URL + WebServiceRequests.uploadRequest.rawValue
                     
                     
                     let data = FileManager.default.contents(atPath: item.path)
                     
                     
                     if(data == nil)
                     {
                         print("data not found to upload")
                     }
                     
                     
                     try formData.appendPart(withFileURL: theURL!, name: "files[]", fileName: item.name.lowercased(), mimeType: item.type)
                 }
                 
             }
             catch {
                 print("error appending file data: \(error)")
             }
             
             
             formData.appendPart(withForm: type.data(using: String.Encoding.utf8)!, name: "type")
             
             
            
             print(formData)
         }, error: nil)
         let sessionManager = AFURLSessionManager(sessionConfiguration: URLSessionConfiguration.default)
         
         let appDelegate = UIApplication.shared.delegate as! AppDelegate
         let reachable = appDelegate.reach
         if reachable!.connection == .wifi || reachable!.connection == .cellular {
             
             let hud = JGProgressHUD(style: .light)
             hud.indicatorView = JGProgressHUDPieIndicatorView()
             hud.detailTextLabel.text = "0% Complete"
             hud.textLabel.text = "Uploading"
             
             DispatchQueue.main.async {
                 //hud.show(in: view)
                 banner.show(queuePosition: QueuePosition.front, bannerPosition: BannerPosition.top)
                 
             }
             
             
             
             let task = sessionManager.uploadTask(withStreamedRequest: request as URLRequest, progress: { (progress) in
                 DispatchQueue.main.async {
                     ProgressHelper.incrementHUD(hud, progress: (Int(progress.fractionCompleted * 100)), withView: view)
                     customeView.progressView.progress = Float(CGFloat(progress.fractionCompleted) )
                     customeView.label.text = "\((Int(progress.fractionCompleted * 100)))% Completed"
                     customeView.imageView.image = #imageLiteral(resourceName: "male_profile_picture.png")
                 }
                 
                 print(progress.fractionCompleted)
                 
             }) { (response, data, error) in
                 
                 if error != nil {
                     DispatchQueue.main.async {
                         
                         banner.dismiss()
                         hud.dismiss(afterDelay: 1.0)
                         
                         completation([String:Any](),NSMutableArray())
                     }
                 }else{
                     
                     
                     print("responce: \(data)")
                     let res = data as? [String:Any]
                     let code = res!["code"] as? String
                     
                 
                     let datares = res!["data"] as! [Any]
                     let photosArray = NSMutableArray()
                     for item in datares {
                         let item = item as! [String: Any]
                         let newPhoto = Gallery()
                         newPhoto.name = String(describing: item["name"] ?? "")
                         newPhoto.type = String(describing: item["mime"] ?? "")
                         newPhoto.size = String(describing: item["size"] ?? "")
                         newPhoto.type = String(describing: item["type"] ?? "")
                         
                         photosArray.add(newPhoto)
                         
                         
                     }
                     let message = res!["message"] as? String
                     print(message)
                     DispatchQueue.main.async {
                         
                         banner.dismiss()
                         hud.dismiss(afterDelay: 1.0)
                         
                         completation([String:Any](),photosArray)
                     }
                 }
             }
           
             task.resume()
             //semaphore.wait()
             print(result)
             //completation(result)
         }else{
             DispatchQueue.main.async {
                 
                 banner.dismiss()
                 ProgressHelper.showSimpleHUD(withView: view, text: "no internet connection")
             }
             
             print("no internet connection")
             completation([String:Any](),NSMutableArray())
         }
         
         
     }*/
     static func sha256(data : Data) -> Data {
         var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
         data.withUnsafeBytes {
             _ = CC_SHA256($0, CC_LONG(data.count), &hash)
         }
         return Data(bytes: hash)
     }

     static func isInternetConnetcionEnabled() -> Bool{
         
         var is_internet_con = true
         let appdelegate = UIApplication.shared.delegate as! AppDelegate
         let reachability = appdelegate.reach
         switch reachability?.connection {
         case .wifi:
             print("wan status ")
         case .cellular:
             print("wifi status ")
         default:
             is_internet_con = false
             print("no internet connection")
             
     
         }
         
         return is_internet_con
     }
 }
/*
  class Downloader {
     class func load(filePath: URL ,viewController: UIViewController , url: URL, completion: @escaping (Data?) -> Void) {
          let sessionConfig = URLSessionConfiguration.default
          let session = URLSession(configuration: sessionConfig)
          let request = try! URLRequest(url: url, method: .get)

          let task = session.dataTask(with: request){ (data, response, error) in
              if (error == nil) {
                  // Success
                  if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                                    
                     print("Success: \(statusCode)")
                     
                     do {
                         try data?.write(to: filePath, options: .atomic)
                        
                      
                     } catch (let writeError) {
                         print("error writing file \(writeError)")
                     }
                     
                     completion(data)
                     
                 }


                  // This is your file-variable:
                  // data
              }
              else {
                  // Failure
                 completion(nil)
                  print("Failure: %@", error?.localizedDescription);
              }
          }
          task.resume()
      }
  }

  class Downloader1 {
      class func load(url: URL, to localUrl: URL, completion: @escaping () -> ()) {
          let sessionConfig = URLSessionConfiguration.default
          let session = URLSession(configuration: sessionConfig)
          let request = try! URLRequest(url: url, method: .get)

          let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
              if let tempLocalUrl = tempLocalUrl, error == nil {
                  // Success
                  if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                      print("Success: \(statusCode)")
                  }

                  do {
                      try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                     
                      completion()
                  } catch (let writeError) {
                      print("error writing file \(localUrl) : \(writeError)")
                  }

              } else {
                  print("Failure: %@", error?.localizedDescription);
              }
          }
          task.resume()
      }
  }
*/
