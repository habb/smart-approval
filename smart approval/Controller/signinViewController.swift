//
//  signinViewController.swift
//  smart approval
//
//  Created by macbook pro on 05/12/2020.
//  Copyright © 2020 Datacellme. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import LocalAuthentication
import SkyFloatingLabelTextField
import CoreData
import GiFHUD_Swift

class signinViewController: UIViewController ,NVActivityIndicatorViewable , UIGestureRecognizerDelegate {
    
    
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var gesture: UITapGestureRecognizer!
    var selectedURL: String = ""
    var userID: String  = ""
    //@IBOutlet var userProfileView: UIView!
    
    var people: [NSManagedObject] = []
    var isEnabled: [NSManagedObject] = []
    var person: NSManagedObject = NSManagedObject()
    @IBOutlet var biometricImage: UIImageView!
    
    var isStart = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIFHUD.shared.setGif(named: "finalll111.gif")
        //authenticationWithTouchID()
        //token()
        
        
        //self.usernameTextField.text = "salah"
        //self.passwordTextField.text = "saifzone1234"
        /*
         userProfileView.layer.cornerRadius = userProfileView.frame.height / 2
         
         userProfileView.layer.masksToBounds = true
         
         userProfileView.layer.borderColor = UIColor(red: 187/255, green: 156/255, blue: 98/255, alpha: 1.0).cgColor
         
         userProfileView.layer.borderWidth = 5*/
        //authenticate()
        
        loginButton.layer.cornerRadius = 8
        loginButton.layer.masksToBounds = true
        //loginButton.applyGradient(colours: [AppConstants.BROWN_COLOR , AppConstants.LIGHT_BROWN_COLOR])
        gesture.delegate = self
        
        
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
            
        } else {
            print("Portrait")
            
        }
        
        GIFHUD.shared.reset()
        GIFHUD.shared.setGif(named: "finalll111.gif")
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        //1
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            
            return
            
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        
        let fetchRequestIsEnabled = NSFetchRequest<NSManagedObject>(entityName: "Faceid")
        
        //3
        do {
            
            people = try managedContext.fetch(fetchRequest)
            isEnabled = try managedContext.fetch(fetchRequestIsEnabled)
            
            
            
            
            if isEnabled.count > 0 {
                biometricImage.image = UIImage(named: "face-id-black") ///face id
            }else{
                biometricImage.image = UIImage(named: "face-id-black")
            }
            if people.count > 0 {
                person = people[0]
                
                let username = person.value(forKeyPath: "username") as? String ?? "undefiened"
                let password = person.value(forKeyPath: "password") as? String ?? "undefiened"
                
                
                if AppConstants.IS_COMING_FROM_LOGOUT {
                    AppConstants.IS_COMING_FROM_LOGOUT = false
                }else{
                    
                    
                    isStart = true
                    self.authenticationWithTouchID()
                }
                
                
                
                
                
                
                print("username is: \(username) and password is : \(password) ")
            }else {
                
            }
            
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    @IBAction func dismissAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        
        guard let view = touch.view else {return true}
        if view.isKind(of: UIButton.self) {
            return false
        }
        
        return true
    }
    
    func save(username: String , password: String) {
        
        
        
        guard let appDelegate =
                
                UIApplication.shared.delegate as? AppDelegate else {
            
            return
            
        }
        
        
        // 1
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        // 2
        
        let entity = NSEntityDescription.entity(forEntityName: "Person",in: managedContext)!
        
        
        let person = NSManagedObject(entity: entity, insertInto: managedContext)
        
        // 3
        
        person.setValue(username, forKeyPath: "username")
        person.setValue(password, forKeyPath: "password")
        
        // 4
        
        do {
            
            try managedContext.save()
            
            //people.append(person)
            
        } catch let error as NSError {
            
            print("Could not save. \(error), \(error.userInfo)")
            
        }
        
    }
    
    func deleteAllData(_ entity:String) {
        //1
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            
            return
            
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Person")
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                managedContext.delete(objectData)
            }
        } catch let error {
            print("Detele all data in \(entity) error :", error)
        }
    }
    func authenticationWithTouchID() {
        
        let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String
        let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String
        
        
        
        if people.count == 0 {
            
            
            
            let alert = UIAlertController(title: "Error", message: "Please login at least one time !", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        
        
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"
        
        var authorizationError: NSError?
        let reason = "Authentication required to access the secure data"
        
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authorizationError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async() {
                        
                        /*
                         let alert = UIAlertController(title: "Success", message: "Authenticated succesfully!", preferredStyle: UIAlertController.Style.alert)
                         alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                         
                         
                         
                         self.token()
                         
                         }))
                         self.present(alert, animated: true, completion: nil)
                         */
                        AppConstants.saveIsEnabled()
                        self.biometricImage.image = UIImage(named: "face-id-black")
                        self.FaceidAuthenticate()
                        
                        
                        
                        //self.token()
                    }
                    
                } else {
                    // Failed to authenticate
                    guard let error = evaluateError else {
                        
                        let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                        return
                    }
                    
                    DispatchQueue.main.async() {
                        
                        
                        let alert = UIAlertController(title: "Error", message: "Authentication Error", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            
                            
                            //self.token()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    print(error)
                    
                }
            }
        } else {
            
            guard let error = authorizationError else {
                
                
                return
            }
            
            if !isStart {
                userDidClickButton()
            }
            
            /*
             let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
             ac.addAction(UIAlertAction(title: "OK", style: .default))
             self.present(ac, animated: true)*/
            print(error)
        }
    }
    
    @IBAction func userDidClickButton() {
        
        // initialise a pop up for using later
        let alertController = UIAlertController(title: "permission required", message: "Please go to Settings and turn on the permissions", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
        /*
         // check the permission status
         switch(L.authorizationStatus()) {
         case .authorizedAlways, .authorizedWhenInUse:
         print("Authorize.")
         // get the user location
         case .notDetermined, .restricted, .denied:
         // redirect the users to settings
         self.present(alertController, animated: true, completion: nil)
         }*/
    }
    @IBAction func loginAction(_ sender: Any) {
        
        
        AppConstants.APPUSERNAME = usernameTextField.text ?? ""
        AppConstants.PASSWORD = passwordTextField.text ?? ""
        
        
        if AppConstants.APPUSERNAME == "" ||
            AppConstants.PASSWORD == "" {
            
            
            let alert = UIAlertController(title: "Error", message: "Please fill username and password ", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            
        }else{
            
            
            UserDefaults.standard.setValue(usernameTextField.text, forKey: AppConstants.APPUSERNAME)
            UserDefaults.standard.setValue(passwordTextField.text, forKey: AppConstants.PASSWORD)
            
            //deleteAllData("person")
            //save(username: usernameTextField.text ?? "", password: passwordTextField.text ?? "" )
            
            UserDefaults.standard.synchronize()
            let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String
            let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String
            
            
            authenticate()
        }
        
        
    }
    @IBAction func fingerAuthenticate(_ sender: Any) {
        
        isStart = false
        authenticationWithTouchID()
    }
    func authenticate() {
        
        DispatchQueue.main.async {
            
            let size = CGSize(width: 30, height: 30)
            
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
            
            GIFHUD.shared.show()
        }
        
        WebService.Authenticate(username: "", password: "") { (json) in
            
            
            print(json)
            
            DispatchQueue.main.async {
                
                self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            guard let Message = json["Message"] as? String else {return}
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            if let userID = Data["UserID"] as? Int {
                print("userID: '\(userID)'")
                
                
                self.userID = String(describing: userID)
            }
            
            
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    
                    self.performSegue(withIdentifier: "toOTP", sender: self)
                    
                }
            }
            
            
            
            
        }
        
        
    }
    
    
    func FaceidAuthenticate() {
        
        DispatchQueue.main.async {
            
            let size = CGSize(width: 30, height: 30)
            
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
            GIFHUD.shared.show()
            
        }
        
        
        let user = SAIFZONEUser.getSaifZoneUser()
        
        WebService.BiometricAuthentication(UserID: user?.userID ?? "") { (json) in
            
            
            print(json)
            
            DispatchQueue.main.async {
                
                self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            guard let Message = json["Message"] as? String else {return}
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            if let userID = Data["UserID"] as? Int {
                print("userID: '\(userID)'")
                
                
                self.userID = String(describing: userID)
            }
            
            
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    
                    self.userID = user?.userID ?? ""
                    self.performSegue(withIdentifier: "toOTP", sender: self)
                    
                }
            }
            
            
            
            
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toOTP" {
            let dest = segue.destination as! OTPViewController
            dest.userID = self.userID
        }
        if segue.identifier == "toWeb" {
            let destn = segue.destination as! UINavigationController
            let dest = destn.viewControllers[0] as! webViewController
            dest.selectedURL = self.selectedURL
        }
    }
    
    func otp() {
        
        
        DispatchQueue.main.async {
            
            
            let size = CGSize(width: 30, height: 30)
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
            GIFHUD.shared.show()
        }
        WebService.OTP(UserID: "2", OTPCode: "2654") { (json) in
            
            print(json)
            DispatchQueue.main.async {
                //self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            print(json)
            
            
            
            
        }
        
    }
    
    func token() {
        
        
        DispatchQueue.main.async {
            
            
            let size = CGSize(width: 30, height: 30)
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
            GIFHUD.shared.show()
        }
        let user = SAIFZONEUser.getSaifZoneUser()
        WebService.Token(UserID: user?.userID ?? "", Token:  user?.tokenID ?? "") { (json) in
            
            print(json)
            DispatchQueue.main.async {
                //self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            
            guard let Message = json["Message"] as? String else {return}
            
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            guard let url = Data["URL"] as? String else {return}
            
            print(json)
            
            //guard let url = json["url"] as? String else {return}
            
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                self.selectedURL = url
                
                DispatchQueue.main.async {
                    //self.performSegue(withIdentifier: "toWeb", sender: self)
                    
                    
                    let webTabController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webController") as! UINavigationController
                    
                    
                    UserDefaults.standard.set("\(self.selectedURL)" ,forKey: "URL")
                    self.view.window!.rootViewController = webTabController
                }
            }
            
            
            
            
            
        }
        
    }
}
extension UIView {
    
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }
    
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
}
