//
//  webViewController.swift
//  smart approval
//
//  Created by macbook pro on 09/12/2020.
//  Copyright © 2020 Datacellme. All rights reserved.
//

import Foundation
import UIKit
import WebKit
import LocalAuthentication
import CoreData
import GiFHUD_Swift

class webViewController: UIViewController , WKNavigationDelegate {
    
    var isShowenLoaded = true
    
    @IBOutlet var webView: WKWebView!
    var isEnabled: [NSManagedObject] = []
    
    var selectedURL: String  = ""
    
    
    override func viewDidDisappear(_ animated: Bool) {
        isShowenLoaded = true
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        GIFHUD.shared.setGif(named: "finalll111.gif")
        
        checkfaceID()
        
        webView.navigationDelegate = self
        
        
        
        let preferences = WKPreferences()
               
        preferences.javaScriptEnabled = true
        
        let configuration = WKWebViewConfiguration()
        
        configuration.preferences = preferences
        
        
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.configuration.preferences.javaScriptEnabled = true
        webView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
       
        
        
        //webView.contentMode = .scaleToFill
        //webView.sizeToFit()
         //   webView.autoresizesSubviews = true
            
        //self.view = webView
        //UserDefaults.standard.set("\(selectedURL)" ,forKey: "URL")
        
        getData()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isShowenLoaded {
            GIFHUD.shared.show()
        }
    }
    func checkfaceID() {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
              
                    return
        
            }
          
          let managedContext = appDelegate.persistentContainer.viewContext
          
          //2
            
            let fetchRequestIsEnabled = NSFetchRequest<NSManagedObject>(entityName: "Faceid")
          
          //3
          do {
          
           
            isEnabled = try managedContext.fetch(fetchRequestIsEnabled)
            
            
            
            
            if isEnabled.count > 0 {
               
                
            }else{
                authenticationWithTouchID()
            }
            
            
          } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
          }
    }
    
    func getData() {
        
        
        DispatchQueue.main.async {
            guard Utilities().isInternetAvailable() == true else{
                Utilities().showAlert(message: "Please check internet connetion", isRefresh : true,actionMessage : "Refresh", controller: self)
                return
            }
            let urlOther :String = UserDefaults.standard.object(forKey: "URL") as! String
            let url : URL = URL(string : urlOther)!
            let request1 = URLRequest(url: url)
            if self.webView != nil {
                self.webView.load(request1)
            }
        }
        
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let clickedUrl : String = (webView.url?.absoluteString)!
        
        if isShowenLoaded {
            isShowenLoaded = false
            
            GIFHUD.shared.dismiss()
        }
        print("Finished navigating to url \(clickedUrl)");
        
        if clickedUrl.contains("LoggedOut"){
            
            //UserDefaults.standard.set(nil, forKey: AppConstants.SaifZoneUserID)
            //UserDefaults.standard.set(nil, forKey: AppConstants.SaifZoneUserToken)
            
            
            AppConstants.IS_COMING_FROM_LOGOUT = true
            
            let loggedInTabController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as! UINavigationController
            self.view.window!.rootViewController = loggedInTabController
            
            
        }
    }
    
    
    
    func authenticationWithTouchID() {
        
        let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String
        let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String
        
        
        
        
            
            
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"

        var authorizationError: NSError?
        let reason = "Authentication required to access the secure data"

        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authorizationError) {
            
            print("yeeeeees")
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async() {
                        
                        /*
                        let alert = UIAlertController(title: "Success", message: "Authenticated succesfully!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            
                            
                            self.token()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        */
                        AppConstants.saveIsEnabled()
                        //self.token()
                    }
                    
                } else {
                    // Failed to authenticate
                    
                    
                    guard let error = evaluateError else {
                        
                        let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                        return
                    }
                    
                    DispatchQueue.main.async() {
                        
                        
                        let alert = UIAlertController(title: "Error", message: "Authentication Error", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            
                            //self.navigationController?.popViewController(animated: true)
                            //self.token()
                            
                        }))
                        //self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    print(error)
                
                }
            }
        } else {
            
            guard let error = authorizationError else {
                
                
                return
            }
            
            let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            //self.present(ac, animated: true)
            print(error)
        }
    }
}
