//
//  OTPViewController.swift
//  smart approval
//
//  Created by macbook pro on 09/12/2020.
//  Copyright © 2020 Datacellme. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SkyFloatingLabelTextField
import LocalAuthentication
import GiFHUD_Swift


class OTPViewController: UIViewController, NVActivityIndicatorViewable ,UITextFieldDelegate{
    
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var otpText1: UITextField!
    @IBOutlet weak var otpText2: UITextField!
    @IBOutlet weak var otpText3: UITextField!
    @IBOutlet weak var otpText4: UITextField!
    
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var resendEmailOTP: UIButton!
    @IBOutlet var resendOTP: UIButton!
    //@IBOutlet var userProfileView: UIView!
    var selectedURL: String  = ""
    //@IBOutlet var otpTextField: SkyFloatingLabelTextFieldWithIcon!
    var userID: String  = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        GIFHUD.shared.setGif(named: "finalll111.gif")
        //self.otpTextField.text = "2654"
     
        //self.otpTextField.becomeFirstResponder()
        /*
        userProfileView.layer.cornerRadius = userProfileView.frame.height / 2
        
        userProfileView.layer.masksToBounds = true
        
        userProfileView.layer.borderColor = UIColor(red: 187/255, green: 156/255, blue: 98/255, alpha: 1.0).cgColor
        
        userProfileView.layer.borderWidth = 5
        
    */
        
        verifyButton.layer.cornerRadius = 8
        verifyButton.layer.masksToBounds = true
        //otpTextField.textContentType = .oneTimeCode
        
        resendOTP.layer.cornerRadius = 8
        resendOTP.layer.masksToBounds = true
        
        resendEmailOTP.layer.cornerRadius = 8
        resendEmailOTP.layer.masksToBounds = true
        
        addBorderToTextField(textField: otpText1)
        addBorderToTextField(textField: otpText2)
        addBorderToTextField(textField: otpText3)
        addBorderToTextField(textField: otpText4)
        
        otpText1.delegate = self
        otpText2.delegate = self
        otpText3.delegate = self
        otpText4.delegate = self
        
        otpText1.becomeFirstResponder()
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
            
        } else {
            print("Portrait")
            
        }
        
        GIFHUD.shared.reset()
        GIFHUD.shared.setGif(named: "finalll111.gif")
    }
    @IBAction func resendEmailOTPAction(_ sender: Any) {
        EmailOTP()
    }
    func addBorderToTextField(textField: UITextField) {
        textField.layer.cornerRadius = 8
        textField.layer.masksToBounds = true
        textField.layer.borderColor = AppConstants.BROWN_COLOR.cgColor
        textField.layer.borderWidth = 2
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField.text?.count ?? 0 < 1) && (string.count > 0) {
            if textField == otpText1 {
                otpText2.becomeFirstResponder()
            }
            
            if textField == otpText2 {
                otpText3.becomeFirstResponder()
            }
            
            if textField == otpText3 {
                otpText4.becomeFirstResponder()
            }
            
            if textField == otpText4 {
                otpText4.resignFirstResponder()
                textField.text = string
                otp()
                return false
            }
            textField.text = string
            
            return false
        }else if (textField.text?.count ?? 0 >= 1) && (string.count == 0) {
            if textField == otpText2 {
                otpText1.becomeFirstResponder()
            }
            if textField == otpText3 {
                otpText2.becomeFirstResponder()
            }
            if textField == otpText4 {
                otpText3.becomeFirstResponder()
            }
            if textField == otpText1 {
                otpText1.resignFirstResponder()
            }
            textField.text = ""
            return false
        }else if textField.text?.count ?? 0 >= 1 {
            textField.text = string
            return false
        }
        
        
        
        
        return true
    }
    
    @IBAction func verifyAction(_ sender: Any) {
        if otpText1.text?.count == 0 || otpText2.text?.count == 0
            || otpText3.text?.count == 0 || otpText4.text?.count == 0 {
            let alert = UIAlertController(title: "Error", message: "Please fill all fields .", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true, completion: nil)
        }else {
            otp()
        }
        
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func resendAction(_ sender: Any) {
        
        FaceidAuthenticate()
    }
    
    
    func FaceidAuthenticate() {
          
        DispatchQueue.main.async {
               
            let size = CGSize(width: 30, height: 30)
            
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
               
            GIFHUD.shared.show()
        }
          
        
        let user = SAIFZONEUser.getSaifZoneUser()
        
        WebService.BiometricAuthentication(UserID: user?.userID ?? "") { (json) in
            
        
            print(json)
            
            DispatchQueue.main.async {
            
                //self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            guard let Message = json["Message"] as? String else {return}
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            if let userID = Data["UserID"] as? Int {
                print("userID: '\(userID)'")
                   
                
                self.userID = String(describing: userID)
            }
            
            
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                 
                DispatchQueue.main.async {
                     
                 
                    self.userID = user?.userID ?? ""
                    // self.performSegue(withIdentifier: "toOTP", sender: self)
                 
                 }
            }
            
               
           
      
        }
       
    
    }
    func EmailOTP() {
          
        DispatchQueue.main.async {
               
            let size = CGSize(width: 30, height: 30)
            
            //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
               
            GIFHUD.shared.show()
        }
          
        
        //let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String ?? ""
        let user = SAIFZONEUser.getSaifZoneUser()
        WebService.EmailOTP(UserID: user?.userID ?? "") { (json) in
            
        
            print(json)
            
            DispatchQueue.main.async {
            
                //self.stopAnimating(nil)
                GIFHUD.shared.dismiss()
            }
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            guard let Message = json["Message"] as? String else {return}
            guard let Data = json["Data"] as? [String:Any] else {return}
            
           
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                 
                DispatchQueue.main.async {
                     
                 
                    
                 
                 }
            }
            
               
           
      
        }
       
    
    }
    func authenticationWithTouchID() {
        
        let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String
        let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String
        
        
        
        
            
            
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Please use your Passcode"

        var authorizationError: NSError?
        let reason = "Authentication required to access the secure data"

        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authorizationError) {
            
            print("yeeeeees")
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, evaluateError in
                /*
                if success {
                    DispatchQueue.main.async() {
                        
                        /*
                        let alert = UIAlertController(title: "Success", message: "Authenticated succesfully!", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            
                            
                            self.token()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        */
                        AppConstants.saveIsEnabled()
                        self.token()
                    }
                    
                } else {
                    // Failed to authenticate
                    
                    
                    guard let error = evaluateError else {
                        
                        let ac = UIAlertController(title: "Authentication failed", message: "You could not be verified; please try again.", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                        return
                    }
                    
                    DispatchQueue.main.async() {
                        
                        
                        let alert = UIAlertController(title: "Error", message: "Authentication Error", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                            
                            
                            //self.navigationController?.popViewController(animated: true)
                            //self.token()
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    
                    print(error)
                
                }*/
            }
        } else {
            
            guard let error = authorizationError else {
                
                
                return
            }
            
            let ac = UIAlertController(title: "Biometry unavailable", message: "Your device is not configured for biometric authentication.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(ac, animated: true)
            print(error)
        }
    }
    func otp() {
          
        
           DispatchQueue.main.async {
                      
              
               let size = CGSize(width: 30, height: 30)
               //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
               GIFHUD.shared.show()
           }
        
        let otpString = "\(otpText1.text!)\(otpText2.text!)\(otpText3.text!)\(otpText4.text!)"
        
        WebService.OTP(UserID: self.userID, OTPCode: otpString ) { (json) in
            
            print(json)
               DispatchQueue.main.async {
                   //self.stopAnimating(nil)
                   GIFHUD.shared.dismiss()
               }
               
               
            print(json)
               
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            
            guard let Message = json["Message"] as? String else {return}
            
            guard let Data = json["Data"] as? [String:Any] else {return}
            let token = String(describing: Data["Token"] ?? "")
            
            let _ = SAIFZONEUser.init(userID: self.userID, token: token)
            
            
            if ErrorCode < 0 {
                
                
                
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    
                    //deleteAllData("person")
                    //save(username: usernameTextField.text ?? "", password: passwordTextField.text ?? "" )
                    
                    
                    UserDefaults.standard.synchronize()
                    let username = UserDefaults.standard.object(forKey: AppConstants.APPUSERNAME) as? String ?? ""
                    let password = UserDefaults.standard.object(forKey: AppConstants.PASSWORD) as? String ?? ""
                    
                    
                    AppConstants.deleteAllData("person")
                    AppConstants.save(username: username , password: password )
                    
                    /*
                    
                    let alert = UIAlertController(title: "Do you want to enable face id ?", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)*/
                    //self.authenticationWithTouchID()
                    self.token()
                }
            }
            
            
            
               
           
        }
       
    }
    
    func token() {
          
        
           DispatchQueue.main.async {
                      
              
               let size = CGSize(width: 30, height: 30)
               //self.startAnimating(size, message: "Loading ...", messageFont: nil, type: .ballBeat)
              // GIFHUD.shared.show()
           }
        
        let user = SAIFZONEUser.getSaifZoneUser()
        WebService.Token(UserID: user?.userID ?? "", Token:  user?.tokenID ?? "") { (json) in
            
            print(json)
               DispatchQueue.main.async {
                   //self.stopAnimating(nil)
                  // GIFHUD.shared.dismiss()
               }
               
               
            print(json)
               
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            
            guard let Message = json["Message"] as? String else {return}
            
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            guard let url = Data["URL"] as? String else {return}
               
            
            if ErrorCode < 0 {
            
            
            
            DispatchQueue.main.async {
                
                let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    
                }))
                self.present(alert, animated: true, completion: nil)
            
                }
            }else{
                
                self.selectedURL = url
                DispatchQueue.main.async {
                    //self.performSegue(withIdentifier: "toWeb", sender: self)
                 
                 let webTabController =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webController") as! webViewController
                 
                 
                 UserDefaults.standard.set("\(self.selectedURL)" ,forKey: "URL")
                 self.view.window!.rootViewController = webTabController
                }
            }
            
        }
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toWeb" {
            let destn = segue.destination as! UINavigationController
            let dest = destn.viewControllers[0] as! webViewController
            dest.selectedURL = self.selectedURL
        }
    }
}
