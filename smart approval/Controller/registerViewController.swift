//
//  registerViewController.swift
//  smart approval
//
//  Created by haya habbous on 06/05/2022.
//  Copyright © 2022 Datacellme. All rights reserved.
//

import Foundation
import UIKit
import SkyFloatingLabelTextField
import NVActivityIndicatorView
import GiFHUD_Swift
class registerViewController: UIViewController ,NVActivityIndicatorViewable , UIGestureRecognizerDelegate{
    
    @IBOutlet var gesture: UITapGestureRecognizer!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var movileNumberTextField: UITextField!
    @IBOutlet weak var fullnameTextField: UITextField!
    @IBOutlet weak var passwordTextFeild: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        GIFHUD.shared.setGif(named: "finalll111.gif")
        registerButton.layer.cornerRadius = 8
        
        registerButton.layer.masksToBounds = true
        
        //gesture.delegate = self
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            
            
        } else {
            print("Portrait")
            
        }
        
        GIFHUD.shared.reset()
        GIFHUD.shared.setGif(named: "finalll111.gif")
    }
    func validateButton() -> Bool{
        if usernameTextField.text == "" || passwordTextFeild.text == "" || fullnameTextField.text == "" || movileNumberTextField.text == "" {
            
            
            return false
        }
        
        return true
    }
    @IBAction func dismissAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    @IBAction func registerButton(_ sender: Any) {
        if !validateButton() {
            
            let alert = UIAlertController(title: "Error", message: "Please fill all fields ", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                
            }))
            self.present(alert, animated: true, completion: nil)
            
            return
        }
        
        GIFHUD.shared.show()
        WebService.Create(username: usernameTextField.text ?? "", password: passwordTextFeild.text ?? "", fullname: fullnameTextField.text ?? "", mobile: movileNumberTextField.text ?? "") { json in
            print(json)
            DispatchQueue.main.async {
                GIFHUD.shared.dismiss()
            }
            
            
            guard let ErrorCode = json["ErrorCode"] as? Int else {return}
            guard let Message = json["Message"] as? String else {return}
            guard let Data = json["Data"] as? [String:Any] else {return}
            
            if ErrorCode == 0 {
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: Message, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }
}
